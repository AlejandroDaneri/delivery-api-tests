Cuando("el pedido es entregado por {string}") do |repartidor|
  expect(@response.status).to eq(200)
  parsed_response = JSON.parse(@response.body)
  delivered_by = parsed_response['delivered_by']
  expect(delivered_by).to eq(repartidor)
end

Cuando("es calificado con {int}") do |calificacion|
  pending # Write code here that turns the phrase above into concrete actions
end

Cuando("no llueve") do
  @request['rain'] = false
  @response = Faraday.post(WEATHER_URL, @request.to_json, header)
  expect(response.status).to eq(200)
end

Entonces("la comision {float}") do |comision|
  response = Faraday.post(query_commission_url(@order_id), @request.to_json, header)
  expect(response.status).to eq(200)
  parsed_response = JSON.parse(@response.body)
  expect(parsed_response['comission_amount']).to be eq (comision)
end

Cuando("llueve") do
  @request['rain'] = true
  @response = Faraday.post(WEATHER_URL, @request.to_json, header)
  expect(response.status).to eq(200)
end