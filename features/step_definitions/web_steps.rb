
And("the response message is {string}") do |message|
  response = JSON.parse(@response.body)
  expect(response['message']).to eq(message)
end

Then("the response status is {int}") do |status|
  expect(@response.status).to eq(status)
end


Cuando("se registra pero no envia api-ley") do
  @request ||= {}
  @response = Faraday.post(REGISTER_DELIVERY_URL, @request.to_json)  
end

Entonces("obtiene error {int}") do |int|
  expect(@response.status).to eq(403)
  parsed_response = JSON.parse(@response.body)
  expect(parsed_response['message']).to eq 'api-key missing or incorrect'
end